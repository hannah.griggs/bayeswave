#!/bin/bash


# single ifo example with trivial MCMC iterations
BayesWave \
    --ifo H1 --H1-flow 32 \
    --H1-cache LALSimAdLIGO --H1-channel LALSimAdLIGO \
    --trigtime 900000000.00 --srate 512 --seglen 4 --PSDstart 900000000 \
    --PSDlength 1024 --NCmin 2 --NCmax 2 --dataseed 1234 \
    --Niter 500 --outputDir master-test
