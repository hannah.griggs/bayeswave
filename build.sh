#!/bin/bash
set -e

INSTALL_PREFIX=$1

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)
			machine=Linux
      CMAKE=cmake3
      ;;
    Darwin*)
      machine=Darwin
      CMAKE=cmake
      ;;
    *)
      machine="UNKNOWN:${unameOut}"
      echo "Unsupported hostOS: ${machine}"
      exit 1
esac
echo "Host OS is ${machine}. Using $CMAKE"

rm -rf build
mkdir -p build
pushd build
${CMAKE} .. \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=true
${CMAKE} --build . -- VERBOSE=1
${CMAKE} --build . --target install
popd
