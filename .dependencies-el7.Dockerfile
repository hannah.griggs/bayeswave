FROM ligo/base:el7

LABEL name="BayesWave Build Dependencies - EL7" \
      maintainer="James Alexander Clark <james.clark@ligo.org>" \
      support="Base image for buillding BayesWave " \
      date="20190726"

# Yum dependencies
RUN yum upgrade -y && \
      yum install -y cmake3 \
      gcc \
      gcc-c++ \
      git \
      help2man \
      lalapps \
      lal-devel \
      lalframe-devel \
      lalinference-devel \
      lalsimulation-devel \
      python-devel \
      python-ligo-lw \
      rpm-build && \
      yum clean all && \
      rm -rf /var/cache/yum
