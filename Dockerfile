FROM containers.ligo.org/lscsoft/bayeswave/dependencies:el7

LABEL name="BayesWave - EL7" \
      maintainer="James Alexander Clark <james.clark@ligo.org>" \
      support="Reference Platform" \
      date="20190726"

# Directories we may want to bind
RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor /test

# Build and install from RPM built in CI
COPY rpms /rpms
RUN  yum upgrade -y && \
      yum -y localinstall /rpms/*.rpm && \
      rm -rf /rpms && yum clean all

# Python Utils
COPY test/test-bayeswave.sh /test-bayeswave.sh
COPY BayesWaveUtils /tmp/BayesWaveUtils
RUN cd /tmp/BayesWaveUtils && \
      python /tmp/BayesWaveUtils/setup.py install 
RUN rm -rf /tmp/*

# Remove build dependencies
RUN yum remove -y cmake3 \
      gcc \
      gcc-c++ \
      help2man \
      lal-devel \
      lalframe-devel \
      lalinference-devel \
      lalsimulation-devel \
      rpm-build && \
      yum clean all && \
      rm -rf /var/cache/yum

WORKDIR /
ENTRYPOINT ["/bin/bash"]
