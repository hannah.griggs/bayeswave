# Installing BayesWave
There are 3 basic components to the BayesWave software package:
 * C executables (e.g., `BayesWave`, `BayesWavePost` etc)
 * Python post-processing & plotting scripts (`megaplot.py` and friends)
 * BayesWavePipe workflow generation module (for writing HTCondor DAGs)

**Table of Contents**
 1. [Install everything](#install-everything)
 1. [Build & Install C-executables](#installing-c-executables): BayesWave, BayesWavePost et al 
 1. [BayesWaveUtils](#bayeswaveutils): Plotting codes & HTCondor workflow generation / make DAGs
 1. [Continuous Integration and Containerization](#continuous-integration-and-containerization)

## Install Everything

Assume bayeswave install path: `${HOME}/opt/lscsoft/bayeswave`:

```
export BAYESWAVE_REPO=${HOME}/src/lscsoft/bayeswave
export BAYESWAVE_PREFIX=${HOME}/opt/lscsoft/bayeswave
git clone git@git.ligo.org:lscsoft/bayeswave.git ${BAYESWAVE_REPO}
cd ${BAYESWAVE_REPO}
./install.sh ${BAYESWAVE_PREFIX}
```

This configures, builds and installs *all* executables and libraries (i.e.,
including python) to ${BAYESWAVE_PREFIX}.  Upon successful installation, you
should see a message like:
```
*****************************************************************************
  DONE: BayesWave built and installed to: 
      /home/albert.einstein/opt/lscsoft/bayeswave
  To use: 
      source /home/albert.einstein/opt/lscsoft/bayeswave/etc/bayeswave-user-env.sh
*****************************************************************************
```
Source that file per the instructions and your environment will be ready to go.

The directory tree of your bayeswave installation looks something like:
```
├── bin 
│   ├── BayesWave
│   ├── BayesWaveCleanFrame
│   ├── bayeswave_pipe
│   ├── BayesWavePost
│   ├── BayesWaveToLALPSD
│   ├── megaplot.py
│   └── megasky.py
├── etc 
│   └── bayeswave-user-env.sh
└── lib 
    ├── libbayeswave.a
    ├── libbayeswave.la
    ├── libbayeswave.so -> libbayeswave.so.0.0.0
    ├── libbayeswave.so.0 -> libbayeswave.so.0.0.0
    ├── libbayeswave.so.0.0.0
    └── python2.7
        └── site-packages
            ├── bayeswave_pipe
            │   ├── bayeswave_pipe_utils.py
            │   ├── bayeswave_pipe_utils.pyc
            │   ├── __init__.py
            │   └── __init__.pyc
            ├── bayeswave_pipe_examples
            │   ├── LDG-GW150914
            │   │   ├── LDG-GW150914.ini
            │   │   └── makework-LDG-GW150914.sh
            │   ├── LDG-GW150914-singularity
            │   │   ├── LDG-GW150914-singularity.ini
            │   │   └── makework-LDG-GW150914-singularity.sh
            │   └── OSG-GW150914-singularity
            │       ├── makework-OSG-GW150914-singularity.sh
            │       ├── OSG-GW150914-singularity.ini
            │       ├── status.sh
            │       └── times.txt
            ├── bayeswave_plot
            │   ├── find_trig.py
            │   ├── find_trig.pyc
            │   ├── __init__.py
            │   ├── __init__.pyc
            │   ├── readbwb.py
            │   └── readbwb.pyc
            ├── bayeswave_plot_data
            │   ├── BWBweb.css
            │   ├── navigate.js
            │   └── secure_ajax.js
            └── BayesWaveUtils-0.1dev-py2.7.egg-info
```

The following notes may be useful for developers who wish to skip configuration
stages and unncessarily rebuilding/reinstalling unchanged components.  Be aware,
however, that you may want to clean the `src` directory of build products if
you're changing C code.

## Installing C Executables

**Requirements**
 * LALSuite: this is installed globally on LDG clusters.  If you are running on
 (e.g.) CIT this requirement is satisfied.   If you're working on a non-LDG
 machine such as your laptop or local workstation, you should make sure
 LALSuite is installed and your environment is correctly configured.

BayesWave is built using an autoreconf script which automatically configures
and builds a Makefile.  There are two basic ways to build bayeswave executables:

 1. Use the helper script `install.sh` (easy, see above)
 1. Execute build commands manually (harder)


### Manual Build
This procedure is similar to lalsuite.  Again, assume the repository has been
cloned to `BAYESWAVE_SRC`.

First, generate the configuration files:
```
cd ${BAYESWAVE_SRC}/src
autoreconf --verbose --force --install --make
```
Now run configure with any optional flags, such as an installation prefix
like ${HOME}/opt/bayeswave:
```
export BAYESWAVE_PREFIX=${HOME}/opt/bayeswave
./configure --prefix ${BAYESWAVE_PREFIX}
```

Finally, make and install:
```
make
make install
```

This will install the non-python executables and libraries in the `bin` and
`lib` directories of your `BAYESWAVE_PREFIX` as above.  You will then need to
set your environment accordingly.  Note that there is an environment script
`BAYESWAVE_SRC/etc/bayeswave-user-env.sh` to help with this step.

## BayesWaveUtils

**Requirements**
 * Swig-wrapped LALSuite: provides an easy interface for reading `sim-inspiral`
 tables from XML
 * Glue: provides tools for segment generation and HTCondor workflows management.
 * `gw_data_find`: The script makes an external call to `gw_data_find` to
 create frame cache files.

BayesWaveUtils consists of two packages and some supplementary data:
 * bayeswave_plot: plotting utilities, skymaps etc
 * bayeswave_plot_data: auxillary files for webpages (javascript, css, etc)
 * bayeswave_pipe: HTCondor workflow generator
 * bayeswave_pipe_examples: example configuration files for bayeswave_pipe

To install BayesWaveUtils to e.g., BAYESWAVE_PREFIX, enter the BayesWaveUtils
directory and:
```
python setup.py install --prefix ${BAYESWAVE_PREFIX}
```

This will install the executable scripts, modules and data directories as
described in [Install everything](#install-everything).   You will then need to
set your environment accordingly.  Note that there is an environment script
`BAYESWAVE_SRC/etc/bayeswave-user-env.sh` to help with this step.


# Continuous Integration and Containerization
GitLab provides integrated continuous integration (CI) tools which can be used
to build and test code whenever a commit is made (see e.g., "latest PDF" for
    LVC papers hosted on git.ligo.org).

Here, we use the CI tools to automatically build and test BayesWave containers
following every commit.  See
[`.gitlab-ci.yml`](https://git.ligo.org/lscsoft/bayeswave/blob/master/.gitlab-ci.yml)
for the CI script and
[`Dockerfile`](https://git.ligo.org/lscsoft/bayeswave/blob/master/Dockerfile)
for the container build instructions.

## Creating Your Own Docker Images
**Note**: these instructions are intended only as a broad overview.


The gitlab CI tools mean that the general user will probably not require their
own containers.  At the time of writing, however, only the master branch
supports containerization.  If you want to create a container for some
development branch, read on:

The `Dockerfile` in master provides the instructions for docker to create an
image.  Unless you have additional dependencies, the master version should not
require modification.  The procedure for building and deploying docker images
(which are converted to singularity images in cvmfs) is basically:

```
cd ${BAYESWAVE_SRC}
docker login containers.ligo.org
docker build --rm --no-cache -t containers.ligo.org/albert.einstein/bayeswave:branch_name  ./
docker push containers.ligo.org/albert.einstein/bayeswave:master
```

where `BAYESWAVE_SRC` is the root of the BayesWave repository, containing `Dockerfile`.
The image will eventually (perhaps up to about 20 minutes, all being well) arrive at:

```
/cvmfs/ligo-containers.opensciencegrid.org/albert.einstein/bayeswave:branch_name
```

This is now visible at *all* LDG and OSG sites with that CVMFS mount enabled.
In other words, you have just deployed BayesWave to every possible computing
resource in one go.

Note that detailed instructions for authentication can be found in the Registry
item of the gitlab side-bar.

In practice, this is handled most easily by the [CI script](https://git.ligo.org/lscsoft/bayeswave/blob/master/.gitlab-ci.yml).

Alternatively, you may wish to deploy your images to dockerhub, which is
basically github for docker images.  This makes them publicly available
(meaning all BayesWave analyses with data releases are in principle easily
 reproducible by the public without having to directly install lalsuite etc).
in this case, you just need to log in and push to dockerhub instead of the LIGO
container registry:

```
cd ${BAYESWAVE_SRC}
docker login  # defaults to dockerhub
docker build --rm --no-cache -t albert.einstein/bayeswave:branch_name  ./
docker push albert.einstien/bayeswave:master
```



